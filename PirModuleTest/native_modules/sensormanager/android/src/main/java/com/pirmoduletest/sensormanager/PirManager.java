package com.pirmoduletest.sensormanager;

import android.content.Context;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by CPark on 5/8/2018.
 */
public class PirManager implements Runnable{

    private final String UNIX_SOCKET_NAME = "atmosfxsensors";
    private final int BYTES_TO_READ = 256;
    private final int INPUT_BUFFER_SIZE = 1024;


    private Context m_Context;
    private LocalSocket m_LocalSocket;
    private LocalSocketAddress m_SocketAddress;
    private InputStream m_inStream;
    private byte[] m_inBuffer;
    private int m_ReadCount;

    private ArrayList<IPirListener> m_Listeners;


    /*--------------------------------------------------------------------------------------------*/

    public PirManager(Context context){
        this.m_Context = context;
        this.m_SocketAddress = new LocalSocketAddress(UNIX_SOCKET_NAME);
        this.m_LocalSocket = new LocalSocket();
        this.m_inStream = null;
        this.m_inBuffer = new byte[INPUT_BUFFER_SIZE];
        this.m_ReadCount = 0;
        this.m_Listeners = new ArrayList<>();
    }

    /*--------------------------------------------------------------------------------------------*/

    //Open Socket
    public void connect(){
        try{
            this.m_LocalSocket.connect(m_SocketAddress);
            Toast.makeText(this.m_Context, "Socket Connection Successful",Toast.LENGTH_SHORT).show();
        }
        catch(IOException ex){
            System.out.println("Error: Failed to connect pir socket");
            ex.printStackTrace();
        }
    }

    /*--------------------------------------------------------------------------------------------*/

    //Close Socket
    public void close(){
        try{
            this.m_LocalSocket.close();
            Toast.makeText(this.m_Context, "Socket Connection Closed",Toast.LENGTH_SHORT).show();
        }
        catch(IOException ex){
            System.out.println("Error: Failed to close pir socket");
            ex.printStackTrace();
        }
    }

    /*--------------------------------------------------------------------------------------------*/
    //Get InStream
    private void getInputStream(){
        try{
            this.m_inStream = this.m_LocalSocket.getInputStream();
        }
        catch(IOException ex){
            System.out.println("Error: Failed to get input stream");
            ex.printStackTrace();
        }
    }

    /*--------------------------------------------------------------------------------------------*/

    //Add Listener
    public void addPirListener(IPirListener listener){
        this.m_Listeners.add(listener);
    }

    /*--------------------------------------------------------------------------------------------*/

    //Remove Listener
    public void removePirListener(IPirListener listener){
        this.m_Listeners.remove(listener);
        Toast.makeText(this.m_Context, "Pir Listener Removed",Toast.LENGTH_SHORT).show();
    }

    /*--------------------------------------------------------------------------------------------*/

    //Do blocking read on input stream.
    public void run() {
        int bytesRead;

        while(true){
            try{
                this.getInputStream();
                bytesRead = this.m_inStream.read(this.m_inBuffer);

                if(bytesRead > 0){
                    for(IPirListener listener : this.m_Listeners) {
                        listener.receiveBytes(this.m_inBuffer);
                    }
                }
            }
            catch(IOException ex){
                System.out.println("Failed read of input stream");
                ex.printStackTrace();
                this.m_ReadCount++;
                if(this.m_ReadCount > 10){
                    System.out.println("Read failures exceed set limit, ending worker thread loop.");
                    this.m_ReadCount = 0;
                    return;
                }
            }
        }
    }

    /*--------------------------------------------------------------------------------------------*/
}
