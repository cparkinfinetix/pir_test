package com.pirmoduletest.sensormanager;

import android.os.AsyncTask;

/**
 * Created by CPark on 5/23/2018.
 * Runs the Pir event loop as a background process to avoid blocking the Java/React Native UI Thread
 */
public class PirReceiverTask extends AsyncTask<PirManager, Void, Void> {

    @Override
    protected Void doInBackground(PirManager... pirManagers) {
        pirManagers[0].run();
        return null;
    }
}
