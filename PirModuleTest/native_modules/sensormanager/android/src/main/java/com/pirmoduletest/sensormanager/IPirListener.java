package com.pirmoduletest.sensormanager;

/**
 * Created by CPark on 5/21/2018.
 */
public interface IPirListener {
    public void receiveBytes(byte[] array);
}
