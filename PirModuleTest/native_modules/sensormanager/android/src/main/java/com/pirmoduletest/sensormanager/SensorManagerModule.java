package com.pirmoduletest.sensormanager;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class SensorManagerModule extends ReactContextBaseJavaModule implements IPirListener{

    private static final String PIR_RECEIVE_EVENT = "pirReceiveEvent";

    private ReactContext m_Context;
    private PirManager m_PirManager;
    private PirReceiverTask m_PirReceiverTask;
    /*--------------------------------------------------------------------------------------------*/

    public SensorManagerModule ( ReactApplicationContext reactContext ) {
        super( reactContext );

        this.m_PirManager = new PirManager(reactContext);
        this.m_Context = reactContext;
        this.m_PirReceiverTask = new PirReceiverTask();
        RegisterListener();
    }

    /*--------------------------------------------------------------------------------------------*/

    @Override
    public String getName() {
        return "SensorManager";
    }

    /*--------------------------------------------------------------------------------------------*/

    @Override
    public void receiveBytes(byte[] array) {
        System.out.println("Received Pir Signal!!");
        System.out.println(new String(array));

        WritableMap params = Arguments.createMap();
        params.putString("pirEvent", new String(array));

        this.m_Context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(PIR_RECEIVE_EVENT, params);
    }

    /*--------------------------------------------------------------------------------------------*/

    @ReactMethod
    public void Connect(){
        this.m_PirManager.connect();
    }

    /*--------------------------------------------------------------------------------------------*/

    @ReactMethod
    public void Start(){
        this.m_PirReceiverTask.execute(this.m_PirManager);
    }

    /*--------------------------------------------------------------------------------------------*/

    @ReactMethod
    public void RegisterListener(){
        this.m_PirManager.addPirListener(this);
    }

    /*--------------------------------------------------------------------------------------------*/

    @ReactMethod
    public void UnregisterListener(){
        this.m_PirManager.removePirListener(this);
    }

    /*--------------------------------------------------------------------------------------------*/

}