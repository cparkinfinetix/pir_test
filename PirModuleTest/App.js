/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  DeviceEventEmitter,
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity
} from 'react-native';

import {SensorManager} from 'sensormanager'

var PirManager = require("NativeModules").SensorManager;

function onButtonConnect(){
  PirManager.Connect();
  console.warn("Connected!");
}

async function onButtonStart(){
  await PirManager.Start();
  console.warn("Running Receive Data Thread");
}

function onButtonAddListener(){
  DeviceEventEmitter.addListener("pirReceiveEvent", onPirReceive);
  console.warn("Added Event Listener");
}

const onPirReceive = (event) => {
  console.warn("Pir Event Caught. Value Is: " + event.pirEvent.toString());
};


type Props = {};
export default class App extends Component<Props> {
  componentDidMount() {
    //Connect
    //Start
  }


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Pir Native Module Test!
        </Text>

        <View style={styles.buttonHolder}>
          <Button
            onPress={onButtonConnect}
            title="Connect"
          />
        </View>

        <View style={styles.buttonHolder}>
          <Button 
            onPress={onButtonAddListener}
            title="Add Listener"
          />
        </View>

        <View style={styles.buttonHolder}>
          <Button
            onPress={onButtonStart}
            title="Start"
          />
        </View>
        <Text style={styles.welcome}>Setup & Use:</Text>
        <Text style={styles.instructions}>1) Connect - Establish a socket connection with the pir sensor.</Text>
        <Text style={styles.instructions}>2) Add Listener - Add a event emitter listener to receive incoming PIR events.</Text>
        <Text style={styles.instructions}>3) Start - Begin Asyncronously listening for PIR activity. Incoming data will appear as warning popups.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonHolder: {
    width: 150,
    height: 50,
  },
});
